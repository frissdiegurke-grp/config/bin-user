#!/usr/bin/env bash
# Checkout git development branch
# Usage: gd

NEXT=development

git checkout $NEXT 2> /dev/null

echo "Current revision: "`git log -1 --format=oneline`
